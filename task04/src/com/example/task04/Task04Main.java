package com.example.task04;

import org.apache.commons.lang3.StringUtils;
import java.io.FileInputStream;
import java.io.IOException;

public class Task04Main {
    public static void main(String[] args) throws IOException {
        //System.setIn(new FileInputStream("D:\\Java\\IdeaProjects\\7-java-io\\task03\\src\\com\\example\\task03\\input.test"));
        String s = "";
        int c;
        while ((c = System.in.read()) != -1) {
            s = s + (char)c;
        }
        String[] words = StringUtils.split(s," \n");
        double sum=0;
        for(String word:words)
            try {
                sum += Double.parseDouble(word);
            }
            catch(NumberFormatException e){

            }
        System.out.print(String.format("%.6f",sum).replace(",","."));
    }
}
