package com.example.task01;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class Task01Main {
    public static void main(String[] args) throws IOException {

        System.out.println(checkSumOfStream(new ByteArrayInputStream(new byte[]{0x33, 0x45, 0x01})));


    }

    public static int checkSumOfStream(InputStream inputStream) throws IOException {
        if (inputStream==null){
            throw new IllegalArgumentException();
        }
        Integer C=0;
        Integer integer;
        while ((integer = inputStream.read()) != -1) {
            C=Integer.rotateLeft(C,1)^integer;
        }

        return C;
    }
}
