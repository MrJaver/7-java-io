package com.example.task03;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;

public class Task03Main {
    public static void main(String[] args) throws IOException {
        //здесь вы можете вручную протестировать ваше решение, вызывая реализуемый метод и смотря результат
        // например вот так:


        System.out.println(readAsString(new FileInputStream("task03/src/com/example/task03/input.test"), Charset.forName("KOI8-R")));

    }

    public static String readAsString(InputStream inputStream, Charset charset) throws IOException {
        if(inputStream==null)
            throw new IllegalArgumentException();
        int integer;
        ArrayList<Byte> listArr=new ArrayList<>();
        while ((integer = inputStream.read()) != -1) {
            listArr.add((byte)integer);
        }
        Object[] b =  listArr.toArray();
        return new String(toPrimitives(b),charset);
    }
    public static byte[] toPrimitives(Object[] oBytes)
    {
        byte[] bytes = new byte[oBytes.length];

        for(int i = 0; i < oBytes.length; i++) {
            bytes[i] = (byte)oBytes[i];
        }

        return bytes;
    }

}
