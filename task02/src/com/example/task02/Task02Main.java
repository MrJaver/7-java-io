package com.example.task02;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;

public class Task02Main {
    public static void main(String[] args) throws IOException {

        //System.setIn(new FileInputStream("D:\\Java\\IdeaProjects\\7-java-io\\task02\\src\\com\\example\\task02\\input.test"));
       // System.setOut(new PrintStream("D:\\Java\\IdeaProjects\\7-java-io\\task02\\src\\com\\example\\task02\\output.test"));
        Integer integer;
        ArrayList<Integer> listArr=new ArrayList<>();
        while ((integer = System.in.read()) != -1) {
            listArr.add(integer);
        }

        for(int i=0;i< listArr.size()-1;i++){
            if(listArr.get(i) !=13 || listArr.get(i + 1)!=10){
                System.out.print(Character.toChars(listArr.get(i)));
            }
        }
        if(!listArr.isEmpty())
            System.out.print(Character.toChars(listArr.get(listArr.size()-1)));
        // чтобы протестировать свое решение, вам нужно:
        // - направить файл input.test в стандартный ввод программы (в настройках запуска программы в IDE или в консоли)
        // - направить стандартный вывод программы в файл output.test
        // - запустить программу
        // - и сравнить получившийся файл output.test с expected.test
    }
}
